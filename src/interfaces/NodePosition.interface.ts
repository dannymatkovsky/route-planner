export interface NodePosition {
  top: number;
  bottom: number;
}
