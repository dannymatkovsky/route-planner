import { State } from './State.interface';
import { StoreObserver } from './StoreObserver.interface';

export interface Store {
  state: State;
  observers: StoreObserver[];
}
