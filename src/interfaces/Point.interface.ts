import { Location } from './Location.interface';

export interface Point {
  id: number;
  location: Location;
}
