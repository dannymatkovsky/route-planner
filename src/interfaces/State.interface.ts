import { Point } from './Point.interface';

export interface State {
  points: Point[];
}
