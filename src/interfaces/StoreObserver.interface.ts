import { State } from './State.interface';

export type StoreObserver = (state: State) => void;
