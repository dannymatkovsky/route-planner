import React from 'react';
import styles from './RoutePlanner.module.css';
import { PointsListComponent } from '../components/PointsList.component';
import { Map } from '../components/Map.component';

export function RoutePlannerPage() {
  return (
    <div className={styles.root}>
      <div className={styles.listContainer}>
        <PointsListComponent />
      </div>
      <div className={styles.mapContainer}>
        <Map />
      </div>
    </div>
  );
}
