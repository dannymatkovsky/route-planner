import { Point } from '../interfaces/Point.interface';
import { dispatch } from './dispatcher';
import { ADD_POINT, REMOVE_POINT, SET_POINTS } from './actionTypes';

export function setPoints(points: Point[]): void {
  dispatch({ type: SET_POINTS, points });
}

export function removePoint(id: number): void {
  dispatch({ type: REMOVE_POINT, id });
}

export function addPoint(location: Location): void {
  dispatch({ type: ADD_POINT, location });
}
