import { ADD_POINT, REMOVE_POINT, SET_POINTS } from './actionTypes';
import { getStateUpdater } from './store';
import { State } from '../interfaces/State.interface';

export function dispatch({
  type,
  ...payload
}: { type: string } & Record<string, any>) {
  const updateState = getStateUpdater();
  switch (type) {
    case SET_POINTS:
      updateState(
        (state: State): State => ({
          ...state,
          points: payload.points,
        })
      );
      break;
    case REMOVE_POINT:
      updateState(
        (state: State): State => ({
          ...state,
          points: state.points.filter((point) => point.id !== payload.id),
        })
      );
      break;
    case ADD_POINT:
      updateState((state: State): State => {
        console.log(state.points);
        return {
          ...state,
          points: [
            ...state.points,
            {
              id: state.points.reduce(
                (newId, point) => (point.id >= newId ? point.id + 1 : newId),
                1
              ),
              location: payload.location,
            },
          ],
        };
      });
      break;
    default:
      return;
  }
}
