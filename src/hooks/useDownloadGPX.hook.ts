import { useStore } from './useStore.hook';
import { buildGPX, BaseBuilder } from 'gpx-builder';
import { useCallback } from 'react';

export function useDownloadGPX() {
  const { points } = useStore();

  const generateGPX = useCallback(() => {
    // Generate GPX content
    const { Point } = BaseBuilder.MODELS;
    const gpxData = new BaseBuilder();
    gpxData.setSegmentPoints(
      points.map((point) => new Point(point.location.lat, point.location.lng))
    );
    const contents = buildGPX(gpxData.toObject());

    // Download as a file
    const fakeLink = document.createElement('a');
    const file = new Blob([contents], {
      type: 'application/gpx+xml',
    });
    fakeLink.href = URL.createObjectURL(file);
    fakeLink.download = 'route.gpx';
    document.body.appendChild(fakeLink);
    fakeLink.click();
  }, [points]);

  return generateGPX;
}
