import { useCallback, useEffect, useLayoutEffect, useState } from 'react';
import { useStore } from './useStore.hook';
import { setPoints } from '../store/actions';

const step = 45;

function getInitialOffsets(points) {
  return points.reduce(
    (offsets, item, index) => ({ ...offsets, [item.id]: index * step }),
    {}
  );
}

export function useDragPoints() {
  const [draggingId, setDraggingId] = useState(null);
  const { points } = useStore();
  const [initialOffsets, setInitialOffsets] = useState({});
  const [offsets, setOffsets] = useState({});
  const [startPosition, setStartPosition] = useState(null);

  useLayoutEffect(() => {
    setInitialOffsets(getInitialOffsets(points));
  }, [points, setInitialOffsets]);

  const onDragStart = useCallback(
    (event, id) => {
      setDraggingId(id);
      setStartPosition(event.clientY);
    },
    [setDraggingId, setStartPosition]
  );

  const onDrag = useCallback(
    (event) => {
      if (!draggingId) {
        return;
      }
      setOffsets((oldOffsets) => {
        const currentOffsets =
          Object.keys(oldOffsets).length > 0 ? oldOffsets : initialOffsets;
        const draggedOffset =
          initialOffsets[draggingId] + event.clientY - startPosition;
        const shifts = {};
        for (let id in currentOffsets) {
          if (parseInt(id) === draggingId) {
            continue;
          }
          if (
            draggedOffset > currentOffsets[id] &&
            draggedOffset < currentOffsets[id] + step / 2
          ) {
            shifts[id] = currentOffsets[id] + step;
            break;
          }
          if (
            draggedOffset + step / 2 > currentOffsets[id] &&
            draggedOffset < currentOffsets[id]
          ) {
            shifts[id] = currentOffsets[id] - step;
            break;
          }
        }
        return { ...currentOffsets, [draggingId]: draggedOffset, ...shifts };
      });
    },
    [draggingId, startPosition, setOffsets, initialOffsets]
  );

  const onDragEnd = useCallback(() => {
    if (!draggingId) {
      return;
    }
    const pointsAfterDragging = Object.keys(offsets)
      .sort((a, b) => (offsets[a] > offsets[b] ? 1 : -1))
      .map((id) => points.find((point) => point.id === parseInt(id)));
    setPoints(pointsAfterDragging);
    setDraggingId(null);
    setOffsets({});
  }, [setDraggingId, draggingId, setOffsets, offsets, points]);

  useEffect(() => {
    document.addEventListener('mousemove', onDrag);
    document.addEventListener('mouseup', onDragEnd);
    return () => {
      document.removeEventListener('mousemove', onDrag);
      document.removeEventListener('mouseup', onDragEnd);
    };
  }, [onDrag, onDragEnd]);

  return {
    offsets: Object.keys(offsets).length > 0 ? offsets : initialOffsets,
    onDragStart,
  };
}
