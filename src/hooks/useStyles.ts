import { useMemo } from 'react';
import { Styles } from '../interfaces/Styles.interface';

function useStyles(defaultStyles: Styles, attachedStyles: Styles) {
  return useMemo((): Styles => {
    return Object.keys(attachedStyles).reduce(
      (styles, key: string) => {
        styles.key = `${styles.key || ''} ${attachedStyles[key]}`;
        return styles;
      },
      { ...defaultStyles }
    );
  }, [defaultStyles, attachedStyles]);
}

export default useStyles;
