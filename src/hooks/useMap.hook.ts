import { useEffect, useRef } from 'react';
import * as L from 'leaflet';
import { useStore } from './useStore.hook';
import { addPoint } from '../store/actions';
import styles from '../components/Map.module.css';

const markerStyle = {
  radius: 14,
  color: '#000',
  stroke: false,
  fillOpacity: 0.9,
};
const polylineStyle = { color: '#1586e2', weight: 4 };

export function useMap(containerRef) {
  const mapRef = useRef(null);
  const { points } = useStore();

  useEffect(() => {
    const map = L.map(containerRef.current).setView([47.7, 12.5], 14);
    L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
      attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(map);
    map.on('click', (e) => addPoint(e.latlng));
    mapRef.current = map;
    return () => map.remove();
  }, [containerRef]);

  useEffect(() => {
    if (!mapRef.current) return;
    const polyline = L.polyline(
      points.map((point) => point.location),
      polylineStyle
    ).addTo(mapRef.current);

    const markers = [];
    points.forEach((point, index) => {
      const marker = L.circleMarker(Object.values(point.location), markerStyle);
      marker.addTo(mapRef.current);
      marker
        .bindTooltip(index + 1 + '', {
          permanent: true,
          direction: 'center',
          className: styles.markerLabel,
        })
        .openTooltip();
      markers.push(marker);
    });

    return () => {
      polyline.remove();
      markers.forEach((marker) => marker.remove());
    };
  }, [points]);

  return;
}
