import React, { useCallback, useEffect, useRef, useState } from 'react';

export function useSmartScrollbar(dependencies) {
  const ref = useRef<HTMLDivElement>(null);
  const [resized, setResized] = useState([false]);

  const bindScrollbar = useCallback((): {
    ref: React.MutableRefObject<HTMLDivElement>;
    style: React.CSSProperties;
  } => {
    return {
      ref,
      style: {
        overflowY:
          ref.current && ref.current.scrollHeight > ref.current.offsetHeight
            ? 'scroll'
            : 'auto',
      },
    };
  }, [...dependencies, resized]); // eslint-disable-line react-hooks/exhaustive-deps

  // Handle resize event
  useEffect(() => {
    const onResized = () => setResized([true]);
    document.addEventListener('resize', onResized);
    return () => {
      document.removeEventListener('resize', onResized);
    };
  }, []);

  return bindScrollbar;
}
