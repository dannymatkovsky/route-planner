import React from 'react';
import styles from './PointsList.module.css';
import { PointItem } from './PointItem.component';
import { useStore } from '../hooks/useStore.hook';
import { useDragPoints } from '../hooks/useDragPoints.hook';
import { useDownloadGPX } from '../hooks/useDownloadGPX.hook';
import { useSmartScrollbar } from '../hooks/useSmartScrollbar.hook';

export function PointsListComponent() {
  const { points } = useStore();

  const { offsets, onDragStart } = useDragPoints();

  const downloadGPX = useDownloadGPX();

  const bindScrollbar = useSmartScrollbar([points]);

  return (
    <div className={styles.root}>
      <div className={styles.header}>
        <h1>Route builder</h1>
      </div>

      <div className={styles.listContainer} {...bindScrollbar()}>
        {points.length === 0 ? (
          <p>Click on map to place a waypoint.</p>
        ) : (
          <div className={styles.list}>
            {points.map((point) => (
              <PointItem
                point={point}
                key={point.id}
                onDragStart={onDragStart}
                offset={offsets[point.id]}
              />
            ))}
          </div>
        )}
      </div>

      <div className={styles.action}>
        <button
          className={styles.button}
          type="button"
          disabled={points.length === 0}
          onClick={() => downloadGPX()}
        >
          Download your route
        </button>
      </div>
    </div>
  );
}
