import React from 'react';
import styles from './PointItem.module.css';
import { TrashIcon } from '../icons/TrashIcon';
import { Point } from '../interfaces/Point.interface';
import { BarsIcon } from '../icons/BarsIcon';
import { removePoint } from '../store/actions';

interface PointItemProps {
  point: Point;
  offset: number;
  onDragStart: (event: React.MouseEvent<HTMLDivElement>, id: number) => void;
}

export function PointItem({ point, offset, onDragStart }: PointItemProps) {
  return (
    <div
      className={styles.root}
      style={{ top: `${offset}px` }}
      data-id={point.id}
    >
      <div
        className={styles.handle}
        onMouseDown={(event) => onDragStart(event, point.id)}
      >
        <BarsIcon />
      </div>
      <div className={styles.point}>Waypoint {point.id}</div>
      <button
        className={styles.deleteButton}
        type="button"
        onClick={() => removePoint(point.id)}
      >
        <TrashIcon />
      </button>
    </div>
  );
}
