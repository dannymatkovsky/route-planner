import React from 'react';
import { RoutePlannerPage } from '../pages/RoutePlanner.page';

function App() {
  return <RoutePlannerPage />;
}

export default App;
