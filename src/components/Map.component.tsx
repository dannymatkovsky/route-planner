import React, { useRef } from 'react';
import 'leaflet/dist/leaflet.css';
import { useMap } from '../hooks/useMap.hook';

export function Map() {
  const containerRef = useRef<HTMLDivElement>(null);

  useMap(containerRef);

  return <div style={{ height: '100%' }} ref={containerRef}></div>;
}
