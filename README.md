# Cross Country Route Planner

## Description

Simple route planner for cross country hikers. Demo: https://dannymatkovsky.gitlab.io/route-planner/

## Install locally

1. Clone repo.
2. Go to project folder.
3. Run `yarn start`.
